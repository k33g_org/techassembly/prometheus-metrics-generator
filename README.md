# prom-metrics-generator

From: https://github.com/arpendu11/node-prometheus-grafana

 Start the Docker containers:
```bash
docker-compose up -d
```

## Add metrics

- Prometheus should be accessible via [http://localhost:9090](http://localhost:9090)
- Grafana should be accessible via [http://localhost:3000](http://localhost:3000)
  - Go to Dashboards/Manage
  - Chose NodeJS Application Dashboard
- Example Node.js server metrics for monitoring should be accessible via [http://localhost:8080/metrics](http://localhost:8080/metrics)
- Example Node.js slow URL should be accessible via [http://localhost:8080/slow](http://localhost:8080/slow)

### Open monitoring dashboards

Open in your web browser the monitoring dashboards:

- [NodeJS Application Dashboard](http://localhost:3000/d/PTSqcpJWk/nodejs-application-dashboard)
- [High Level Application metrics](http://localhost:3000/d/OnjTYJg7k/high-level-application-metrics)
- [Node Service Level Metrics Dashboard](http://localhost:3000/d/WBxkVyRnz/node-service-level-metrics-dashboard)
- [NodeJS Request Flow Dashboard](http://localhost:3000/d/2Er5E1R7k/nodejs-request-flow-dashboard)
